function validate() {
    var name = document.RegForm.nama;
    var gender = document.RegForm.gender;
    var hobi = document.RegForm.hobi;
    var email = document.RegForm.email;
    var telp = document.RegForm.telp;
    var user = document.RegForm.user;
    var password = document.RegForm.password;

    if (name.value.length <= 0 || !firstUpper(name.value)) {
        alert("Name harus diisi dan huruf pertama harus kapital!");
        name.focus();
        return false;
    }

    if (gender.value.length <= 0) {
        alert("Pilih Jenis Kelamin!");
        name.focus();
        return false;
    }

    var check = false;
    for (var i = hobi.length - 1; i >= 0; i--) {
        if (hobi[i].checked) {
            check = true;
        }
    }
    if (check == false) {
        alert("pilih Hobi!");
        return false;
    }

    if (email.value.length <= 0 || !validateEmail(email.value)) {
        alert("email harus diisi dan gunakan format email!");
        email.focus();
        return false;
    }

    if (telp.value.length <= 0) {
        alert("Telepon harus diisi!");
        telp.focus();
        return false;
    }

    if (user.value.length <= 0 || user.value.length > 10) {
        alert("Username harus diisi dan maksimal karakter adalah 10");
        user.focus();
        return false;
    }

    if (password.value.length <= 0 || password.value.length <= 6) {
        alert("password harus diisi dan minimum karakter adalah 7");
        password.focus();
        return false;
    }

    return alert("Selamat! Berhasil daftar");
}

function hanyaAngka(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))

        return false;
    return true;
}

function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function firstUpper(name) {
    const re = /^[A-Z/]/;
    return re.test(name);
}